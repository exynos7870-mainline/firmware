# Samsung Galaxy J7 Prime

## Contents

|          File           |                    Description                     |
|-------------------------|----------------------------------------------------|
| `bluetooth_bcm.hcd`     | Bluetooth firmware for Broadcom BCM43430A1         |
| `wlansdio_bcm.bin`      | Wi-Fi SDIO firmware for Broadcom BCM43430A1        |
| `wlansdio_bcm.txt`      | Wi-Fi SDIO configuration for Broadcom BCM43430A1   |
